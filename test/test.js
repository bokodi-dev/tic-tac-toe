const assert = require('assert');
const config = Object.freeze(require('../config.js'));
const gameConfig = Object.freeze(require('../app/game-config.js'));

const http = require('http');
const ws = require('ws');
const nodeStatic = require('node-static');
const nodeStaticServer = new nodeStatic.Server('./public');

const Lobby = require('../app/Lobby.js');
const Player = require('../app/Player.js');
const Game = require('../app/Game.js');

// todo more tests

describe('Server', () => {
    describe('HTTP', () => {
        let httpServer = http.createServer((request, response) => {
            // serve static files
            request.addListener('end', () => {
                nodeStaticServer.serve(request, response);
            }).resume();
        });

        before(() => {
            httpServer.listen(config.httpPort, config.ip);
        });

        after(() => {
            httpServer.close();
        });

        it('should return 200', done => {
            http.get(`http://${config.ip}:${config.httpPort}`, response => {
                assert.equal(200, response.statusCode);
                done();
            });
        });
    });

    describe('WebSocket', () => {
        let wsServer = new ws.Server({host: config.ip, port: config.wsPort});

        it('Should connect', function (done) {
            wsServer.on('connection', socket => {
                done();
                socket.close();
            });

            new ws(`ws://${config.ip}:${config.wsPort}`);
        });
    });
});

describe('Game', () => {
    describe('Lobby', () => {
        let player1 = new Player();
        let player2 = new Player();

        player1.rename('John Doe');
        player2.rename('John Doe');

        it('should have unique player names', () => {
            let lobby = new Lobby();

            assert(lobby.join(player1));
            assert(!lobby.join(player2));
        });

        it('should find player by name', () => {
            let lobby = new Lobby();

            lobby.join(player1);

            assert(lobby.getPlayerByName(player1.name) === player1);
        });
    });

    describe('Player', () => {
        let player1 = new Player();
        let player2 = new Player();

        player1.rename('John Doe');
        player2.rename('John Doe');

        it('should be unique despite same name', () => {
            assert(!player1.equals(player2));
        });
    });

    describe('TicTacToe', () => {
        let player1 = new Player();
        let player2 = new Player();

        it('should let valid moves', () => {
            let game = new Game(player1, player2);

            assert(game.isValidMove(0, 0));
        });

        it('should not let invalid moves', () => {
            let game = new Game(player1, player2);

            assert(!game.isValidMove(game.cols, game.rows));
        });

        it('should check when the game goal is hit', () => {
            let game = new Game(player1, player2);

            for (let i = 0; i < gameConfig.GAME_GOAL; ++i) {
                game.add(game.getPlayerSymbol(player1), i, 0);
            }

            assert(game.check(player1, 0, 0));
        });

        it('should not check when the game goal is not hit', () => {
            let game = new Game(player1, player2);

            assert(!game.check(player1, 0, 0));
        });
    });
});
