# Tic Tac Toe - Homework

## Config
> `./config.js` - server config

> `./app/game-config.js` - game config

## Usage
> `npm install` - installs dependencies

> `node server.js` - runs the server

> `npm test` - runs the tests

> `node client.js` - runs client



## Megjegyzés az ellenőrzést végző fejlesztőnek
> A projectet 3 fő részre lehet osztani. A public mappában van a webes kliens, a gyökér könyvtárban lévő `client.js` a CLI kliens, minden egyéb fájl a szerverhez tartozik.

> A programhoz nem használtam `socket.io`-t és `express`-t. Felhasznált node modulok a `node-static` és a `ws`, tesztekhez a `mocha`. Kliens oldalon nem használtam semmilyen külső forrást js-hez.

> Az opcionális feladatok közül az AI részt kihagytam. 2016-ban már csináltam egy tictactoe-t mesterséges intelligenciával, ez az alábbi linken [megtekinthető](https://github.com/bokodi-archive/2016/tree/master/gomoku).

> ui: kimondottan tetszett a feladat.
