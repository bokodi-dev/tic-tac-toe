const WebSocket = require('ws');
const config = Object.freeze(require('./config.js'));

const stdIn = process.stdin;
stdIn.setEncoding('utf-8');

const commandsMap = new Map();

commandsMap.set('set_name', '{value}');
commandsMap.set('list_players', '');
commandsMap.set('challenge_player', '{value}');
commandsMap.set('spectate_player', '{value}');
commandsMap.set('move', '{x} {y}');

stdIn.on('data', data => {
    let [command, ...args] = data.split(/\s/);

    if (!commandsMap.has(command)) {
        return;
    }

    let wsData = { event: command };
    let argumentMap = commandsMap.get(command).match(/{(.*?)}/g) || [];

    argumentMap.forEach((match, i) => {
        let key = match.replace(/[{}]/g, '');

        wsData[key] = args[i];
    });

    ws.send(JSON.stringify(wsData));
});

class Game {
    constructor() {
        this.rows = 0;
        this.cols = 0;
        this.cells = [];
    }

    add(type, x, y) {
        this.cells[this.index(x, y)] = type;
    }

    get(x, y) {
        return this.cells[this.index(x, y)];
    }

    setContent(cells, rows, cols) {
        this.resize(rows, cols);
        this.cells = cells;
    }

    resize(rows, cols) {
        this.cells = [];
        this.rows = rows;
        this.cols = cols;
    }

    index(x, y) {
        return this.cols * y + x;
    }
}

const CROSS = 1;
const CIRCLE = 2;

const marks = new Map();
marks.set(CROSS, 'x');
marks.set(CIRCLE, 'o');

class GameRenderer {
    constructor(game) {
        this.game = game;
    }

    plain(delimiter = '|') {
        let game = this.game;
        let map = '';

        for (let y = 0; y < game.rows; ++y) {
            for (let x = 0; x < game.cols; ++x) {
                map += delimiter;
                map += marks.get(game.get(x, y)) || ' ';
            }

            map += delimiter;
            map += '\r\n';
        }

        return map;
    }
}

const ws = new WebSocket(`ws://${config.ip}:${config.wsPort}`);

const game = new Game();
const gameRenderer = new GameRenderer(game);

ws.on('open', function open() {
    console.log('Available commands:');
    console.log('>set_name JohnDoe');
    console.log('>list_players');
    console.log('>challenge_player JaneDoe');
    console.log('>spectate_player JaneDoe');
    console.log('>move 2 5');
});

ws.on('message', message => {
    let data;

    try {
        data = JSON.parse(message);
    } catch(error) {
        return console.error('Invalid data');
    }

    // todo: this is ugly
    switch (true) {
        case data.event === 'message':
            console.log(data.message);
            break;

        case data.event === 'name_set':
            console.log('Your name has been set successfully');
            break;

        case data.event === 'game_started':
            console.log('Game started!');
            game.resize(data.rows, data.cols);
            console.log(gameRenderer.plain());
            break;

        case data.event === 'game_move':
            game.add(data.type, data.x, data.y);
            console.log(`New move: ${data.x} ${data.y}`);
            console.log(gameRenderer.plain());
            break;

        case data.event === 'board':
            game.setContent(data.cells, data.rows, data.cols);
            console.log(gameRenderer.plain());
            break;

        case data.event === 'players':
            console.log('Available players:');
            console.dir(data.players);
            break;
    }
});
