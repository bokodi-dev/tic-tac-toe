class Player {
    constructor() {
        this.id = Symbol();
        this.name = '';
    }

    rename(name) {
        this.name = name;
    }

    equals(player) {
        return this.id === player.id;
    }
}

module.exports = Player;
