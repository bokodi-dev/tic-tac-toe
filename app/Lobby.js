const Game = require('./Game.js');

class Lobby {
    constructor() {
        this.games = [];
        this.players = [];

        this.playerMap = new WeakMap();
    }

    join(player) {
        if (player.name && this.isNameTaken(player.name)) {
            return false;
        }

        this.players.push(player);

        return true;
    }

    leave(player) {
        let index = this.players.indexOf(player);

        if (index !== -1) {
            this.players.splice(index, 1);
        }

        if (this.playerMap.has(player)) {
            this.endGame(this.playerMap.get(player));
        }
    }

    createGame(player1, player2) {
        let game = new Game(player1, player2);

        this.games.push(game);

        this.playerMap.set(player1, game);
        this.playerMap.set(player2, game);

        return game;
    }

    endGame(game) {
        let index = this.games.indexOf(game);

        if (index !== -1) {
            this.games.splice(index, 1);
        }

        game.players.forEach(player => this.playerMap.delete(player));

        game.over();
    }

    isNameTaken(name) {
        return this.players.some(player => player.name === name);
    }

    isPlayerAvailable(player) {
        return !this.getPlayerGame(player);
    }

    getPlayerByName(name) {
        return this.players.find(player => player.name === name);
    }

    getPlayerGame(player) {
        return this.playerMap.get(player);
    }
}

module.exports = Lobby;
