const EventEmitter = require('events');

class WsHandler extends EventEmitter {
    constructor() {
        super();

        this.availableEvents = new Set();

        this.sockets = new WeakMap();
        this.channels = new WeakMap();
    }

    parseMessage(message, player, socket) {
        let data;

        try {
            data = JSON.parse(message);
        } catch(error) {
            return void console.error(error.message);
        }

        console.log(data); // todo remove

        let ev = `event.${data.event}`;

        if (!this.availableEvents.has(ev)) {
            return;
        }

        this.emit(ev, player, socket, data);
    }

    broadCast(channelKey, data) {
        let sockets = this.channels.get(channelKey);

        if (!sockets) return;

        sockets.forEach(socket => WsHandler.send(socket, data));
    }

    static send(socket, data) {
        socket.send(JSON.stringify(data));
    }

    static sendMessage(socket, message) {
        WsHandler.send(socket, { event: 'message', message });
    }
}

module.exports = WsHandler;
