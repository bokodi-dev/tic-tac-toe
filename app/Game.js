const EventEmitter = require('events');
const CONFIG = Object.freeze(require('./game-config.js'));

class Game extends EventEmitter {
    constructor(player1, player2) {
        super();

        this.rows = CONFIG.ROWS;
        this.cols = CONFIG.COLS;
        this.isRunning = true;
        this.cells = [];
        this.players = [player1, player2];
        this.spectators = [];
        this.round = 1;
        this.current = 0;
    }

    isValidMove(x, y) {
        return this.isCell(x, y) && !this.hasCellContent(x, y);
    }

    isCell(x, y) {
        return (
            x >= 0 &&
            y >= 0 &&
            x < this.cols &&
            y < this.rows
        );
    }

    next() {
        if (this.isOver()) {
            return false;
        }

        ++this.round;

        if (++this.current >= this.players.length) {
            this.current = 0;
        }

        return true;
    }

    isFull() {
        return this.cells.some(cell => cell === undefined);
    }

    isOver() {
        return !this.isRunning;
    }

    isPlayersTurn(player) {
        return this.getCurrentPlayer() === player;
    }

    getCurrentPlayer() {
        return this.players[this.current];
    }

    move(player, x, y) {
        let symbol = this.getPlayerSymbol(player);

        if (!symbol) {
            throw new Error();
        }

        this.add(symbol, x, y);
        this.next();
    }

    index(x, y) {
        return this.cols * y + x;
    }

    hasCellContent(x, y) {
        return this.cells[this.index(x, y)] !== undefined;
    }

    getCellContent(x, y) {
        return this.cells[this.index(x, y)];
    }

    getPlayerSymbol(player) {
        return CONFIG.SYMBOLS[this.players.indexOf(player)];
    }

    add(symbol, x, y) {
        this.cells[this.index(x, y)] = symbol;
    }

    check(player, x, y) {
        let symbol = this.getPlayerSymbol(player);
        let directions = [[0, 1], [1, 1], [1, 0], [1, -1]];
        let i = directions.length;

        for (; i--;) {
            let [dirX, dirY] = directions[i];
            let hits = 1;
            let r;

            r = 1;
            // count hits, dir = next
            while (this.getCellContent(x + dirX * r, y + dirY * r) === symbol && this.isCell(x + dirX * r, y + dirY * r)) {
                ++r;
                ++hits;
            }

            r = 1;
            // count hits, dir = previous
            while (this.getCellContent(x - dirX * r, y - dirY * r) === symbol && this.isCell(x - dirX * r, y - dirY * r)) {
                ++r;
                ++hits;
            }

            if (hits >= CONFIG.GAME_GOAL) {
                return true;
            }
        }

        return false;
    }

    over() {
        this.isRunning = false;
    }
}

module.exports = Game;
