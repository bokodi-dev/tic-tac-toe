const GAME_GOAL = 5;

const SYMBOL_CROSS = 1;
const SYMBOL_CIRCLE = 2;

const ROWS = 10;
const COLS = 10;

const SYMBOLS = [
    SYMBOL_CROSS,
    SYMBOL_CIRCLE,
];

module.exports = {
    GAME_GOAL,

    ROWS,
    COLS,

    SYMBOL_CROSS,
    SYMBOL_CIRCLE,

    SYMBOLS,
};
