const Lobby = require('./Lobby.js');
const Player = require('./Player.js');
const WsHandler = require('./WsHandler.js');

const lobby = new Lobby();
const gameHandler = new WsHandler();

const EVENT_SET_NAME = 'event.set_name';
const EVENT_LIST_PLAYERS = 'event.list_players';
const EVENT_CHALLENGE_PLAYER = 'event.challenge_player';
const EVENT_SPECTATE_PLAYER = 'event.spectate_player';
const EVENT_MOVE = 'event.move';

gameHandler.availableEvents.add(EVENT_SET_NAME);
gameHandler.availableEvents.add(EVENT_LIST_PLAYERS);
gameHandler.availableEvents.add(EVENT_CHALLENGE_PLAYER);
gameHandler.availableEvents.add(EVENT_SPECTATE_PLAYER);
gameHandler.availableEvents.add(EVENT_MOVE);

gameHandler.on('start', socket => {
    let player = new Player(socket);

    lobby.join(player);
    gameHandler.sockets.set(player, socket);

    socket.on('message', message => {
        message = String(message).trim();

        if (!message) return;

        gameHandler.parseMessage(message, player, socket);
    });

    socket.on('close', function close() {
        lobby.leave(player);
        gameHandler.sockets.delete(player);
    });
});

gameHandler.on(EVENT_SET_NAME, (player, socket, data) => {
    let name = data.value;

    if (!name) {
        return WsHandler.sendMessage(socket, 'Name is required.');
    }

    if (lobby.isNameTaken(name)) {
        return WsHandler.sendMessage(socket, 'Name is already taken.');
    }

    player.rename(name);

    WsHandler.send(socket, {
        event: 'name_set',
    });
});

gameHandler.on(EVENT_LIST_PLAYERS, (player, socket) => {
    let players = lobby.players;
    let playerNames = players.map(p => p.name);
    let nameList = playerNames.filter(playerName => playerName && playerName !== player.name);

    WsHandler.send(socket, { event: 'players', players: nameList });
});

gameHandler.on(EVENT_CHALLENGE_PLAYER, (player, socket, data) => {
    let name = data.value;

    if (!player.name) {
        return WsHandler.sendMessage(socket, 'Your name is not set.');
    }

    let opponent = lobby.getPlayerByName(name);

    if (!opponent) {
        return WsHandler.sendMessage(socket, 'Player not found.');
    }

    if (!lobby.isPlayerAvailable(opponent)) {
        return WsHandler.sendMessage(socket, 'Player is not available.');
    }

    if (!lobby.isPlayerAvailable(player)) {
        return WsHandler.sendMessage(socket, 'You are already in a game.');
    }

    if (player.equals(opponent)) {
        return WsHandler.sendMessage(socket, 'Cannot challenge yourself.');
    }

    // todo accept challenge

    let game = lobby.createGame(player, opponent);

    gameHandler.channels.set(game, [
        gameHandler.sockets.get(player),
        gameHandler.sockets.get(opponent),
    ]);

    gameHandler.broadCast(game, {
        event: 'game_started',
        rows: game.rows,
        cols: game.cols,
    });
});

gameHandler.on(EVENT_SPECTATE_PLAYER, (player, socket, data) => {
    let name = data.value;
    let targetPlayer = lobby.getPlayerByName(name);

    if (!targetPlayer) {
        return WsHandler.sendMessage(socket, 'Player not found.');
    }

    if (!lobby.isPlayerAvailable(player)) {
        return WsHandler.sendMessage(socket, 'You are already in a game.');
    }

    if (lobby.isPlayerAvailable(targetPlayer)) {
        return WsHandler.sendMessage(socket, 'Player is not in a game.');
    }

    if (player.equals(targetPlayer)) {
        return WsHandler.sendMessage(socket, 'Cannot spectate yourself.');
    }

    let game = lobby.getPlayerGame(targetPlayer);

    game.spectators.push(player);

    WsHandler.send(socket, {
        event: 'board',
        rows: game.rows,
        cols: game.cols,
        cells: game.cells,
    });
});

gameHandler.on(EVENT_MOVE, (player, socket, data) => {
    let { x, y } = data;

    x = Number(x);
    y = Number(y);

    if (!Number.isInteger(x) || !Number.isInteger(y)) {
        return WsHandler.sendMessage(socket, 'Invalid parameters.');
    }

    let game = lobby.getPlayerGame(player);

    if (!game) {
        return WsHandler.sendMessage(socket, 'No game found.');
    }

    if (!game.isPlayersTurn(player)) {
        return WsHandler.sendMessage(socket, 'Not your turn.');
    }

    if (!game.isValidMove(x, y)) {
        return WsHandler.sendMessage(socket, 'Not a valid move.');
    }

    if (game.isOver()) {
        return WsHandler.sendMessage(socket, 'Game is over.');
    }

    game.move(player, x, y);

    let type = game.getPlayerSymbol(player);

    gameHandler.broadCast(game, {
        event: 'game_move',
        type, x, y,
    });

    game.spectators.forEach(spectator => {
        let spectatorSocket = gameHandler.sockets.get(spectator);

        WsHandler.send(spectatorSocket, {
            event: 'game_move',
            type, x, y,
        });
    });

    if (game.check(player, x, y)) { // check if there is a winner before check isFull
        gameHandler.broadCast(game, { event: 'message', message: `Game over! ${player.name} won` });
        lobby.endGame(game);
    } else if (game.isFull()) {
        gameHandler.broadCast(game, { event: 'message', message: `Game over! Draw` });
        lobby.endGame(game);
    }
});

module.exports.wsHandler = gameHandler;
