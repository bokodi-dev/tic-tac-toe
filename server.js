const http = require('http');
const ws = require('ws');
const nodeStatic = require('node-static');
const config = Object.freeze(require('./config.js'));
const { wsHandler } = require('./app/main.js');

const nodeStaticServer = new nodeStatic.Server('./public');

const wsServer = new ws.Server({ host: config.ip, port: config.wsPort });

const httpServer = http.createServer((request, response) => {
    // serve static files
    request.addListener('end', () => {
        nodeStaticServer.serve(request, response);
    }).resume();
});

wsServer.on('connection', socket => {
    wsHandler.emit('start', socket);
});

httpServer.listen(config.httpPort, config.ip);
