(function() {
    'use strict';

    var CROSS = 1;
    var CIRCLE = 2;

    function TicTacToe(canvasElement) {
        this.cols = 0;
        this.rows = 0;
        this.size = 25;

        this.canvasElement = canvasElement;
        this.ctx = this.canvasElement.getContext('2d');
    }

    TicTacToe.prototype.reset = function() {
        this.cells = [];

        this.clear();
        this.resize();
        this.render();
    };

    TicTacToe.prototype.clear = function() {
        this.ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    };

    TicTacToe.prototype.resize = function() {
        var availWidth = document.documentElement.offsetWidth * 0.9 | 0;
        var availHeight = document.documentElement.offsetHeight * 0.9 | 0;

        this.size = Math.max(25, Math.min(availWidth / this.cols | 0, availHeight / this.rows | 0));
    };

    TicTacToe.prototype.render = function() {
        var i, il, b;

        this.setSize();
        this.grid();

        for (i = 0, il = this.cells.length; i < il; i++) {
            if (b = this.cells[i]) {
                this.draw(b, i % this.cols, i / this.cols | 0);
            }
        }
    };

    TicTacToe.prototype.setSize = function() {
        this.canvasElement.width = this.cols * this.size;
        this.canvasElement.height = this.rows * this.size;
    };

    TicTacToe.prototype.add = function(type, x, y) {
        this.cells[this.index(x, y)] = type;
    };

    TicTacToe.prototype.setContent = function(cells, rows, cols) {
        this.cells = cells;
        this.rows = rows;
        this.cols = cols;

        this.resize();
        this.render();
    };

    TicTacToe.prototype.grid = function() {
        var x = this.cols;
        var y = this.rows;
        var s = this.size;

        for (;--x;) {
            this.ctx.moveTo(x * s, 0);
            this.ctx.lineTo(x * s, this.canvasElement.height);
        }

        for (;--y;) {
            this.ctx.moveTo(0, y * s);
            this.ctx.lineTo(this.canvasElement.width, y * s);
        }

        this.ctx.stroke();
    };

    TicTacToe.prototype.index = function(x, y) {
        return this.cols * y + x;
    };

    TicTacToe.prototype.isValid = function(x, y) {
        return !this.cells[this.index(x, y)] && this.isCell(x, y);
    };

    TicTacToe.prototype.isCell = function(x, y) {
        return (
            x >= 0 &&
            y >= 0 &&
            x < this.cols &&
            y < this.rows
        );
    };

    TicTacToe.prototype.draw = function(item, x, y) {
        this.ctx.beginPath();

        switch(item) {
            case CROSS:
                this.ctx.strokeStyle = 'red';
                this.ctx.lineWidth = 2;
                this.ctx.moveTo((x + 0.8) * this.size, (y + 0.2) * this.size);
                this.ctx.lineTo((x + 0.2) * this.size, (y + 0.8) * this.size);
                this.ctx.moveTo((x + 0.2) * this.size, (y + 0.2) * this.size);
                this.ctx.lineTo((x + 0.8) * this.size, (y + 0.8) * this.size);
                this.ctx.stroke();

                break;

            case CIRCLE:
                this.ctx.strokeStyle = 'blue';
                this.ctx.lineWidth = 2;
                this.ctx.arc((x + 0.5) * this.size, (y + 0.5) * this.size, this.size * 0.35 | 0, 0, Math.PI * 2);
                this.ctx.stroke();

                break;
        }
    };

    window.TicTacToe = TicTacToe;
}());
