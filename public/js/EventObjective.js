(function() {
    'use strict';

    function EventObjective() {
        this.eventList = {};
    }

    var eventObjectiveProto = EventObjective.prototype = Object.create(null);
    eventObjectiveProto.constructor = EventObjective;

    eventObjectiveProto.addEventListener = function(type, listener) {
        var events = this.eventList[type] || (this.eventList[type] = []);

        events.push(listener);

        return this;
    };

    eventObjectiveProto.hasEventListener = function(type) {
        var events = this.eventList[type];

        return !!events && events.length > 0;
    };

    eventObjectiveProto.removeEventListener = function(type, listener) {
        var events = this.eventList[type],
            index;

        if (events !== undefined) {
            index = events.indexOf(listener);

            if (index !== -1) events.splice(index, 1);
        }

        return this;
    };

    eventObjectiveProto.dispatchEvent = function(type) {
        var events = this.eventList[type],
            args, i, il;

        if (events !== undefined) {
            args = Array.prototype.slice.call(arguments, 1);

            for (i = 0, il = events.length; i < il; i++) {
                events[i].apply(this, args);
            }
        }

        return this;
    };

    window.EventObjective = EventObjective;
}());
