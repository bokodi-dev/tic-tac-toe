(function() {
    'use strict';

    var ticTacToe = new TicTacToe(document.getElementById('board'));
    var ticTacToeClient = new WsClient('ws://127.0.0.1:8090');

    ticTacToeClient.addEventListener('message', function(data) {
        alert(data.message);
    });

    ticTacToeClient.addEventListener('name_set', function() {
        document.getElementById('settings').style.display = 'none';
        document.getElementById('lobby').style.display = '';
    });

    ticTacToeClient.addEventListener('game_started', function(data) {
        ticTacToe.rows = data.rows;
        ticTacToe.cols = data.cols;

        ticTacToe.reset();

        document.getElementById('game').style.display = '';
    });

    ticTacToeClient.addEventListener('board', function(data) {
        ticTacToe.setContent(data.cells, data.rows, data.cols);
        document.getElementById('game').style.display = '';
    });

    ticTacToeClient.addEventListener('game_move', function(data) {
        ticTacToe.add(data.type, data.x, data.y);
        ticTacToe.draw(data.type, data.x, data.y);
    });

    ticTacToeClient.addEventListener('players', function(data) {
        var playersElement = document.getElementById('players');

        playersElement.innerHTML = '';

        if (!data.players instanceof Array) {
            return;
        }

        data.players.forEach(function(player) {
            var liElement = document.createElement('li');
            var nameElement = document.createElement('b');
            var challengeButton = document.createElement('button');
            var spectateButton = document.createElement('button');

            nameElement.innerHTML = player;
            challengeButton.innerHTML = 'challenge';
            spectateButton.innerHTML = 'spectate';
            challengeButton.dataset.eventType = 'challenge_player';
            spectateButton.dataset.eventType = 'spectate_player';

            [challengeButton, spectateButton].forEach(function(button) {
                button.type = 'button';
                button.classList.add('btn', 'btn-default', 'btn-sm');
                button.dataset.targetPlayer = player;
            });

            liElement.appendChild(nameElement);
            liElement.appendChild(challengeButton);
            liElement.appendChild(spectateButton);

            playersElement.appendChild(liElement);
        });
    });

    document.getElementById('board').addEventListener('click', function(e) {
        var posX = e.offsetX;
        var posY = e.offsetY;

        var x = posX / ticTacToe.size | 0;
        var y = posY / ticTacToe.size | 0;

        // client side check to save network data
        if (!ticTacToe.isValid(x, y)) {
            return;
        }

        ticTacToeClient.send({
            event: 'move',
            x: x,
            y: y,
        });
    });

    document.getElementById('players').addEventListener('click', function(e) {
        var targetElement = e.target;
        var eventType = targetElement.dataset.eventType;
        var targetPlayer = targetElement.dataset.targetPlayer;

        if (!eventType || !targetPlayer) {
            return;
        }

        ticTacToeClient.send({
            event: eventType,
            value: targetPlayer
        });
    });

    document.getElementById('set-name').addEventListener('click', function() {
        var name = document.getElementById('name').value;

        ticTacToeClient.send({
            event: 'set_name',
            value: name
        });
    });

    document.getElementById('refresh-players').addEventListener('click', function() {
        ticTacToeClient.send({
            event: 'list_players'
        });
    });

    window.addEventListener('resize', function() {
        ticTacToe.resize();
        ticTacToe.render();
    });
}());
