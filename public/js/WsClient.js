(function() {
    'use strict';

    function WsClient(url) {
        EventObjective.call(this);

        this.socket = new WebSocket(url);

        this.socket.addEventListener('message', function(messageEvent) {
            this.parseMessage(messageEvent);
        }.bind(this));
    }

    WsClient.prototype = Object.create(EventObjective.prototype);
    WsClient.prototype.constructor = WsClient;

    WsClient.prototype.send = function(data) {
        try {
            data = JSON.stringify(data);
        } catch (error) {
            return console.error(error);
        }

        this.socket.send(data);
    };

    WsClient.prototype.parseMessage = function(messageEvent) {
        var data;

        try {
            data = JSON.parse(messageEvent.data);
        } catch(error) {
            return console.error(error.message);
        }

        if (!data || !this.hasEventListener(data.event)) {
            return;
        }

        this.dispatchEvent(data.event, data);
    };

    window.WsClient = WsClient;
}());
